﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemaNumero1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Definicion de Variables
            int num1, num2, sum;

            // Asignacion de variable
            num1 = 0;
            num2 = 0;
            sum = 0;

            // Cuerpo del Programa
            Console.WriteLine("Sistema para saber que numero es mayor segun  dos Numeros Enteros.");
            Console.WriteLine("Ingrese el Numero1 :");

            // Ingreso de valor numero por teclado.
            num1 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Ingrese el Numero2 :");

            // Ingreso de valor numero por teclado.
            num2 = int.Parse(Console.ReadLine());

            // Estructura IF-ELSE.
            if(num1>num2)
            {
                Console.WriteLine("El numero 1 ingresado es mayor que el numero 2 y el valor es :" + num1);
            }
            else
            {
                if(num1==num2)
                {
                    Console.WriteLine("El numero 1 ingresado es igual que el numero 2 y el valor es :" + num1);
                }
                else
                {
                    Console.WriteLine("El numero 2 ingresado es mayor que el numero 1 y el valor es :" + num2);
                }
            }

            Console.ReadKey();
        }
    }
}
