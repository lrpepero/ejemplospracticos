﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MenuConsola
{
    public class Program
    {
        static void Main(string[] args)
        {
            DibujarMenu();
        }

        #region MisMetodos
        static void DibujarMenu()
        {
            ConsoleKeyInfo op;
            #region do / while principal
            do
            {
                Console.Clear();
                Console.WriteLine("MENU PRINCIPAL");
                Console.WriteLine("===============");
                Console.WriteLine("A- SUMA:");
                Console.WriteLine("B- RESTA:");
                Console.WriteLine("C- MULTIPLICACION");
                Console.WriteLine("D- DIVISION");
                Console.WriteLine("F1- SALIR");
                Console.WriteLine("Ingrese la oprcion que desee seleccionar:");

                op = Console.ReadKey();
                #region Switcheo de opciones en base al ingreso por tecaldo.
                switch (op.Key)
                {
                    case ConsoleKey.A:

                        Console.Clear();
                        Console.WriteLine("PROGRAMA PARA REALIZAR UNA SUMA ENTRE DOS NUMEROS");
                        int a, b,suma;
                        a = 0;
                        b = 0;
                        Console.WriteLine("INGRESE EL PRIMER NUMERO A SUMAR: ");
                        a = int.Parse(Console.ReadLine());

                        Console.WriteLine("INGRESE EL SEGUNDO NUMERO A SUMAR: ");
                        b = int.Parse(Console.ReadLine());

                        suma = Sumar(a, b);
                        Console.WriteLine("LA SUMA DE AMBOS NUMEROS ES : " + suma);
                        Console.ReadKey();
                        break;

                    case ConsoleKey.B:
                        Console.Clear();
                        Console.WriteLine("ELEGISTE OPCION B DEL TECLADO");
                        int ma = 0, mb = 0, resta = 0;
                        Console.WriteLine("Ingrese el primero numero para restar");
                        ma = int.Parse(Console.ReadLine());
                        Console.WriteLine("Ingrese el segundo numero para restar");
                        mb = int.Parse(Console.ReadLine());
                        resta = Restar(ma, mb);
                        Console.WriteLine("la resta de ambos numeros es : " + resta);

                        Console.ReadKey();
                        break;

                    case ConsoleKey.C:

                        Console.WriteLine("ELEGISTE OPCION C DEL TECLADO");
                        Console.ReadKey();
                        break;

                    case ConsoleKey.D:

                        Console.WriteLine("ELEGISTE OPCION C DEL TECLADO");
                        Console.ReadKey();
                        break;

                    case ConsoleKey.F1:

                        Console.Clear();
                        Console.WriteLine("ADIOS");
                        Console.ReadKey();
                        break;
                }
                #endregion
            } while (op.Key != ConsoleKey.F1);
            #endregion

        }

        static int Sumar(int a, int b)
        {
            int c = 0;
            c = a + b;
            return c;
          
        }
        static int Restar(int ma, int mb)
        {
            int mc = 0;
            mc = ma - mb;
            return mc;
        }
        #endregion
    }
}
